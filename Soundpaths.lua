local LSM = LibStub("LibSharedMedia-3.0") 

-- ----- 
-- BACKGROUND 
-- ----- 

-- ----- 
--  BORDER 
-- ---- 

-- -----
--   FONT
-- -----

-- -----
--   SOUND
-- -----
LSM:Register("sound", "|cFF00FFFFWC2:Axe|r", [[Interface\Addons\SharedMedia_Celestian\sound\Axe.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Building Explode|r", [[Interface\Addons\SharedMedia_Celestian\sound\Bldexpl3.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Blood Lust|r", [[Interface\Addons\SharedMedia_Celestian\sound\Blodlust.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Bombs are Great|r", [[Interface\Addons\SharedMedia_Celestian\sound\bombs-great.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Bow Fire|r", [[Interface\Addons\SharedMedia_Celestian\sound\Bowfire.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Bow Hit|r", [[Interface\Addons\SharedMedia_Celestian\sound\Bowhit.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Catapult|r", [[Interface\Addons\SharedMedia_Celestian\sound\Catapult.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Death Groan|r", [[Interface\Addons\SharedMedia_Celestian\sound\DIE.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Don't Push it|r", [[Interface\Addons\SharedMedia_Celestian\sound\dont-pushit.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Dwarf: Achk|r", [[Interface\Addons\SharedMedia_Celestian\sound\Dwhat2.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:I come to serve|r", [[Interface\Addons\SharedMedia_Celestian\sound\Eready.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Exorcism|r", [[Interface\Addons\SharedMedia_Celestian\sound\Exorcism.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Explode|r", [[Interface\Addons\SharedMedia_Celestian\sound\Explode.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Far am you|r", [[Interface\Addons\SharedMedia_Celestian\sound\far-am-u.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Fireball|r", [[Interface\Addons\SharedMedia_Celestian\sound\Fireball.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Fire hit|r", [[Interface\Addons\SharedMedia_Celestian\sound\Firehit.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Flame shield|r", [[Interface\Addons\SharedMedia_Celestian\sound\Flamshld.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Ive got a flying machine|r", [[Interface\Addons\SharedMedia_Celestian\sound\got-flying.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Haste|r", [[Interface\Addons\SharedMedia_Celestian\sound\Haste.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Captured|r", [[Interface\Addons\SharedMedia_Celestian\sound\Hcapture.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Holy Vision|r", [[Interface\Addons\SharedMedia_Celestian\sound\Holyvisn.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Are you still touching me|r", [[Interface\Addons\SharedMedia_Celestian\sound\Hpissed2.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Awaiting Orders|r", [[Interface\Addons\SharedMedia_Celestian\sound\Hready.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Your command|r", [[Interface\Addons\SharedMedia_Celestian\sound\Hwhat1.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:At once sire|r", [[Interface\Addons\SharedMedia_Celestian\sound\Hyessir4.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Im alive|r", [[Interface\Addons\SharedMedia_Celestian\sound\im-alive.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Invisible|r", [[Interface\Addons\SharedMedia_Celestian\sound\Invisibl.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:I love blowing things up|r", [[Interface\Addons\SharedMedia_Celestian\sound\love-blowing.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Morph|r", [[Interface\Addons\SharedMedia_Celestian\sound\Morph.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Pig squealing|r", [[Interface\Addons\SharedMedia_Celestian\sound\Pigpissd.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Ready to serve|r", [[Interface\Addons\SharedMedia_Celestian\sound\Psready.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Slow|r", [[Interface\Addons\SharedMedia_Celestian\sound\Slow.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:Tilt one back with me dog|r", [[Interface\Addons\SharedMedia_Celestian\sound\tilt-one-back.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:What!|r", [[Interface\Addons\SharedMedia_Celestian\sound\what.ogg]])
LSM:Register("sound", "|cFF00FFFFWC2:I warned you|r", [[Interface\Addons\SharedMedia_Celestian\sound\Wzpissd3.ogg]])

-- -----
--   STATUSBAR
-- -----
